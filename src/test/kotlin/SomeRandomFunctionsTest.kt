import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals


internal class SomeRandomFunctionsTest {

    @Nested
    @DisplayName("Testing grade functions")
    inner class GradesTests {

        private var someRandomFunctions: SomeRandomFunctions? = null

        @BeforeEach
        fun beforeEach() {
            someRandomFunctions = SomeRandomFunctions()
        }

        @AfterEach
        fun afterEach() {
            someRandomFunctions = null
        }

        @Test
        fun `receive input with decimals and round up the number`() {
            assertEquals(6, someRandomFunctions?.getGradeRounded(5.8))
        }

        @Disabled("Exception has to be added to function to properly handle grades lower than 5.")
        @Test
        fun `if input is 4 with decimals number should round down`() {
            assertEquals(4, someRandomFunctions?.getGradeRounded(4.9))
        }

        @Test
        fun `receive grade Int input and return grade in String`() {
            assertEquals("No presentat", someRandomFunctions?.selectGrade(0))
        }

        @Test
        fun `when input equals above 10 or below 0 return error message`() {
            assertAll("Input above 10 or below 0",
                { assertEquals("input is not valid.", someRandomFunctions?.selectGrade(11)) },
                { assertEquals("input is not valid.", someRandomFunctions?.selectGrade(0)) }
            )
        }

    }

    @Nested
    @DisplayName("Testing functions that work with numbers from SomeRandomFunctions class")
    inner class TestingNumberFunctions {


        private var someRandomFunctions: SomeRandomFunctions? = null

        @BeforeEach
        fun beforeEach() {
            someRandomFunctions = SomeRandomFunctions()
        }

        @AfterEach
        fun afterEach() {
            someRandomFunctions = null
        }

        @Test
        fun `receives input a & b, returns a list comprosided by values in between`() {
            assertEquals(listOf(1, 2, 3, 4), someRandomFunctions?.getListOfNumbers(1, 4))
        }

        @Test
        fun `receives list and returns value`() {
            assertEquals(0, someRandomFunctions?.minValue(listOf(1, 2, 3, 4, 5)))
        }

    }

    @Nested
    @DisplayName("Testing functions that work with strings from SomeRandomFunctions class")
    inner class TestingStringFunctions {


        private var someRandomFunctions: SomeRandomFunctions? = null

        @BeforeEach
        fun beforeEach() {
            someRandomFunctions = SomeRandomFunctions()
        }

        @AfterEach
        fun afterEach() {
            someRandomFunctions = null
        }

        @Test
        fun `receives string and char, returns list of positions matched with char`() {
            assertEquals(listOf(1, 2, 3, 4), someRandomFunctions?.getCharPositionsString("hellohello", "b"[0]))
        }

        @Test
        fun `receives string and char, returns first occurrence of char`() {
            assertEquals(0, someRandomFunctions?.firstOccurrencePosition("hello", "e"[0]))
        }

    }


}