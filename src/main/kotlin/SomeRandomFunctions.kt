/**
 * This class includes a collection of random functions.
 * @author Leonel Matte
 * @since 1.0
 */
class SomeRandomFunctions {

    /**
     * Creates a list between two numbers. The list includes both ending and starting numbers.
     * @return list of numbers between [a] as the starting number [b] as the ending number.
     */
    fun getListOfNumbers(a: Int, b: Int): List<Int> {
        val num = mutableListOf<Int>()
        for (i in a..b) {
            num.add(i)
        }
        return num
    }

    /**
     * Functions takes in [a] list of Ints which is iterated upon to match the following if statements: when i<b and when i==b.
     * @return if one of the previous if statements has been matched the function returns a value of b.
     */
    fun minValue(a: List<Int>): Int {
        var b = 0

        for (i in a.indices) {
            if (i < b) {
                b = i
            } else if (i == b) {
                b = i
            }
        }
        return b
    }


    /**
     * Function receives [i] this is the grade in Int format.
     * @return A grade in the format of a string; 1 would correspond to "Insuficient"
     */
    fun selectGrade(i: Int): String {
        return when (i) {
            0 -> "No presentat"
            1, 2, 3, 4 -> "Insuficient"
            10 -> "Excellent + MH"
            5, 6 -> "Suficient"
            7 -> "Notable"
            else -> "input is not valid"
        }
    }


    /**
     * Function receives [abc] which is iterated until the value in the position equals [b] the corresponding position is added to charPositions.
     * @return charPositions: which is a list of the positions where [b] has been matched in the String [abc]
     */
    fun getCharPositionsString(abc: String, b: Char): List<Int> {
        val charPositions = mutableListOf<Int>()
        for (i in 0 until abc.lastIndex) {
            if (abc[i] == b) {
                charPositions.add(i)
            }
        }
        return charPositions
    }

    /**
     * Function takes in [abc] checks if [b] has been matched.
     * @return -1 if [b] has not been matched. if [b] has been matched function returns first occurrence of matched.
     */
    fun firstOccurrencePosition(abc: String, b: Char): Int {
        for (i in abc.indices) {
            if (abc[i] == b) {
                return i
            }
        }
        return -1
    }

    /**
     * Function receives [grade] with decimals and rounds up the number.
     * @return [grade] converted to integer.
     * if decimal part is <= to 0.5 the number will be rounded up higher. If [grade] = 4.9 , grade will be rounded up down.
     */
    fun getGradeRounded(grade: Double): Int {
        return grade.toInt()
    }

}